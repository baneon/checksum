# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!
#
# WARNING! The generated file contains a high level of configuration
# you may experience errors depending on the version or platform where you run.

from PyQt5.QtCore import QCoreApplication, QMetaObject, QSize
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5 import QtWidgets


class UiMainWindow(object):

    def __init__(self):
        self.width = 500
        self.height = 480
        self.title = 'CheckSum'
        self.window_icon =\
            'C:\\Users\\edwin\\Mis proyectos\\CheckSum\\data\\icon\\icon.ico'


    def setupUi(self, MainWindow):
        MainWindow.setObjectName('MainWindow')
        MainWindow.resize(self.width, self.height)
        MainWindow.setMinimumSize(QSize(self.width, self.height))
        MainWindow.setMaximumSize(QSize(self.width, self.height))

        self.icon = QIcon()
        self.icon.addPixmap(QPixmap(self.window_icon), QIcon.Normal, QIcon.Off)

        MainWindow.setWindowIcon(self.icon)

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName('centralwidget')

        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName('gridLayout_2')

        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setObjectName('plainTextEdit')
        self.plainTextEdit.setReadOnly(1)
        self.gridLayout_2.addWidget(self.plainTextEdit, 1, 0, 1, 1)

        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(10, 10, 10, 10)
        self.horizontalLayout.setObjectName('horizontalLayout')

        self.openButton = QtWidgets.QPushButton(self.centralwidget)
        self.openButton.setObjectName('openButton')
        self.horizontalLayout.addWidget(self.openButton)

        self.clearButton = QtWidgets.QPushButton(self.centralwidget)
        self.clearButton.setObjectName('clearButton')
        self.horizontalLayout.addWidget(self.clearButton)

        self.gridLayout_2.addLayout(self.horizontalLayout, 2, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)

        self.initBox()


    def retranslateUi(self, MainWindow):
        _translate = QCoreApplication.translate
        MainWindow.setWindowTitle(_translate('MainWindow', self.title))
        self.openButton.setText(_translate('MainWindow', 'Abrir'))
        self.clearButton.setText(_translate('MainWindow', 'Limpiar'))


    def initBox(self):
        self.box = QtWidgets.QMessageBox()
        self.box.setIcon(QtWidgets.QMessageBox.Question)
        self.box.setWindowIcon(self.icon)
        self.box.setWindowTitle('Cerrar Checksum')
        self.box.setText('¿Estás seguro de que quieres cerrar la aplicación?')
        self.box.setStandardButtons(
            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        self.box.setDefaultButton(QtWidgets.QMessageBox.No)
        self.buttonY = self.box.button(QtWidgets.QMessageBox.Yes)
        self.buttonY.setText('Si')
        self.box.button(QtWidgets.QMessageBox.No).setText('No')
