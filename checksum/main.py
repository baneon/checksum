#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2020 baneon - MIT License
# See `LICENSE` included in the source distribution for details.

import sys
import os
import hashlib

from PyQt5.QtWidgets import QApplication, QFileDialog, QMainWindow
from data.ui.main import UiMainWindow


class Window(QMainWindow):

    def __init__(self):
        super(Window, self).__init__()
        self.ui = UiMainWindow()
        self.ui.setupUi(self)
        self.ui.openButton.clicked.connect(self.openFileNamesDialog)
        self.ui.clearButton.clicked.connect(self.clear)


    def clear(self):
        self.ui.plainTextEdit.clear()


    def closeEvent(self, event):
        self.ui.box.exec_()
        if self.ui.box.clickedButton() == self.ui.buttonY:
            event.accept()
        else:
            event.ignore()


    def openFileNamesDialog(self):
        title = "Abrir uno o más archivos"
        accept = "Todos los Archivos (*)"
        files, _ = QFileDialog.getOpenFileNames(self, title, "", accept)
        for file in files:
            base = os.path.basename(file)
            checksum = self.sha256_checksum(file)
            string = base + "\n" + checksum + "\n"
            self.ui.plainTextEdit.appendPlainText(string)


    def sha256_checksum(self, filename, block_size=6553):
        sha256 = hashlib.sha256()
        with open(filename, "rb") as file:
            for block in iter(lambda: file.read(block_size), b""):
                sha256.update(block)
        return sha256.hexdigest()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec())
